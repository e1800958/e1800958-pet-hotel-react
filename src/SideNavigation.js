import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class SlideNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {isOpen: false};
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return <div className="wrapper">
    <div className="sidebar">
        <h2>Pet Hotel</h2>
        <ul>
            <li><Link to="/bookings">Booking</Link></li>
            <li><Link to="/users">User</Link></li>
        </ul> 
        <div className="social_media">
          <a href="#"><i className="fab fa-facebook-f"></i></a>
          <a href="#"><i className="fab fa-twitter"></i></a>
          <a href="#"><i className="fab fa-instagram"></i></a>
        </div>
    </div>
  <div className="main_content">
      
  </div>
</div>;
  }
}