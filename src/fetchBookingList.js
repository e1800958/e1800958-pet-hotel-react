import React, { Component } from 'react';
import BookingList from './BookingList';

class fetchBookingList extends Component {

  constructor(props) {
    super(props);
    this.state = {bookings: [], isLoading: true};
    // this.remove = this.remove.bind(this);
  }

  async componentDidMount() {
      fetch('https://e1800958-pet-hotel.herokuapp.com/bookings',{
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: "Basic " + btoa("user:password123"),
        },
      })
         .then(response => response.json())
        .then(data => this.setState({bookings: data, isLoading: false}));
    
    
  }


  render() {
    const {bookings, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    return (
        <BookingList bookings= {bookings} isLoading= {false}/>
    );
  }
}

export default fetchBookingList;