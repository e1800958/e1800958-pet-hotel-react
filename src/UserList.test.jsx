import React from 'react';
import { configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import UserList from './UserList';
configure({adapter: new Adapter()});

const users=[
    {
      "id": 1,
      "name": "Huy Nguyen",
      "email": "abc",
      "phone": "123",
      "status": "enabled"
    },
    {
      "id": 2,
      "name": "Hoang Nguyen",
      "email": "qwe",
      "phone": "678",
      "status": "enabled"
    }
  ];

describe("Testing UserList component existance", () =>{

    it("The table exists" ,  () => {
        const wrapper= shallow(<UserList users= {users} isLoading= {false}/>);
        expect(wrapper.find("div").find(".mt-4").length).toBe(1);
    });
    it("The table has 2 rows" ,  () => {
      const wrapper= shallow(<UserList users= {users} isLoading= {false}/>);
      expect(wrapper.find("div").find(".mt-4").find("tbody").find("tr").length).toBe(2);
  });
});



