import React, { Component } from 'react';
import { Table, Button, ButtonGroup } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import SlideNavigation from './SideNavigation'

class BookingList extends Component {

  constructor(props) {
    super(props);
    this.state = {bookings: this.props.bookings, isLoading: this.props.isLoading};
  }

  async remove(id) {
    await fetch(`https://e1800958-pet-hotel.herokuapp.com/booking/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(() => {
      let updatedBookings = [...this.state.bookings].filter(i => i.id !== id);
      this.setState({bookings: updatedBookings});
    });
  }


  render() {
    const {bookings, isLoading} = this.state;
    const bookingList = bookings.map(booking => {
      return <tr key={booking.id}>
        <td style={{whiteSpace: 'nowrap'}}>{booking.id}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.owner}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.pet}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.period_start}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.period_start}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.status}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.fee}</td>
        <td style={{whiteSpace: 'nowrap'}}>{booking.time}</td>
        <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/booking/" + booking.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(booking.id)}>Delete</Button>
        </ButtonGroup>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <SlideNavigation/>
        <div className="contain" >
          <h3>List of Bookings</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">Id</th>
              <th width="20%">Owner</th>
              <th width="20%">Pet</th>
              <th width="20%">Arrival</th>
              <th width="20%">Departure</th>
              <th width="20%">Status</th>
              <th width="20%">Total fee</th>
              <th width="20%">Created at</th>
              {/* <th width="20%">Created at</th> */}
              <th width="20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            {bookingList}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default BookingList;