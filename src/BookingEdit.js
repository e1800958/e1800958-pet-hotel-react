import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';

class BookingEdit extends Component {

  emptyItem = {
    id: '',
    owner: '',
    period_start: '',
    period_end: '',
    time: '',
    pet: '',
    status:'',
    fee:'',
  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    console.log(this.props.match.params.id)
    if (this.props.match.params.id !== 'new') {
      const booking = await (await fetch('https://e1800958-pet-hotel.herokuapp.com/booking/'+ this.props.match.params.id,{
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: "Basic " + btoa("user:password123"),
        },
      })).json();
      this.setState({item: booking});
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const id = target.id;
    let item = {...this.state.item};
    item[id] = value;
    this.setState({item});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;

    await fetch('https://e1800958-pet-hotel.herokuapp.com/booking', {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        Authorization: "Basic " + btoa("user:password123"),
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/bookings');
  }

  render() {
    const {item} = this.state;
    const title = <h2>{item.id ? 'Edit Booking' : 'Add Booking'}</h2>;

    return <div>
      <AppNavbar/>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="name">Owner</Label>
            <Input type="text" name="owner" id="owner" value={item.owner || ''}
                   onChange={this.handleChange} autoComplete="owner"/>
          </FormGroup>
          <FormGroup>
            <Label for="address">Pet</Label>
            <Input type="text" name="pet" id="pet" value={item.pet || ''}
                   onChange={this.handleChange} autoComplete="pet"/>
          </FormGroup>
          <FormGroup>
            <Label for="city">Arrival</Label>
            <Input type="text" name="period_start" id="period_start" value={item.period_start || ''}
                   onChange={this.handleChange} autoComplete="period_start-le"/>
          </FormGroup>
          <FormGroup>
            <Label for="city">Departure</Label>
            <Input type="text" name="period_end" id="period_end" value={item.period_end || ''}
                   onChange={this.handleChange} autoComplete="period_end"/>
          </FormGroup>
          <FormGroup>
            <Label for="city">Total Fee</Label>
            <Input type="text" name="fee" id="fee" value={item.user_id || ''}
                   onChange={this.handleChange} autoComplete="fee"/>
          </FormGroup>
          <FormGroup>
            <Button color="primary" type="submit">Save</Button>{' '}
            <Button color="secondary" tag={Link} to="/bookings">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default BookingEdit;