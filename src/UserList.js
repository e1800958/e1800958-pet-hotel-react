import React, { Component } from 'react';
import { Button, ButtonGroup, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import SlideNavigation from './SideNavigation'

class UserList extends Component {

  constructor(props) {
    super(props);
    this.state = {users: this.props.users, isLoading: this.props.isLoading};
  }
  render() {
    const {users, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const userList = users.map(user => {
    //  const address = `${group.address || ''} ${group.city || ''} ${group.stateOrProvince || ''}`;
      return <tr key={user.id}>
        <td style={{whiteSpace: 'nowrap'}}>{user.id}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.name}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.email}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.phone}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.status}</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/booking-user/" + user.id}>See the bookings</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <SlideNavigation/>
        <div class="contain" >
          <h3>List of Users</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">Id</th>
              <th width="20%">Name</th>
              <th width="20%">Email</th>
              <th width="20%">Phone</th>
              <th width="20%">Status</th>
              <th width="20%">Action</th>
            </tr>
            </thead>
            <tbody>
            {userList}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default UserList;