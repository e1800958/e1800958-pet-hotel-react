import React from 'react';
import { configure, shallow, mount} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import BookingEdit from './BookingEdit';
configure({adapter: new Adapter()});

describe("Testing BookingEdit component existance", () =>{

    it("The form existance" ,  () => {
        const wrapper= shallow(<BookingEdit/>);
        expect(wrapper.find("div").find("Container").find("Form").length==1).toBe(true);
    });
});