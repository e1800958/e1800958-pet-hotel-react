import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import SlideNavigation from './SideNavigation'

class Home extends Component {
  render() {
    return (
      <div>
        <AppNavbar/>
          <SlideNavigation/>
      </div>
    );
  }
}

export default Home;