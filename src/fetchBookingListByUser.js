import React, { Component } from 'react';
import BookingListByUser from './BookingListByUser';

class fetchBookingListByUser extends Component {

  constructor(props) {
    super(props);
    this.state = {bookings: [], isLoading: true};
    // this.remove = this.remove.bind(this);
  }

  async componentDidMount() {
    
      fetch(`https://e1800958-pet-hotel.herokuapp.com/booking-user/${this.props.match.params.id}`,{
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: "Basic " + btoa("user:password123"),
        },
      })
         .then(response => response.json())
        .then(data => this.setState({bookings: data, isLoading: false}));

  }

  render() {
    const {bookings, isLoading} = this.state;
    

    if (isLoading) {
      return <p>Loading...</p>;
    }
    return (
      <BookingListByUser bookings={bookings} isLoading={false}/>
    );
  }
}

export default fetchBookingListByUser;