import React from 'react';
import ReactDOM from 'react-dom';
import {mount, configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import BookingList from './BookingList';
import nock from 'nock';
import waitUntil from 'async-wait-until';
configure({adapter: new Adapter()});

const bookings=[
  {
    "id": 1,
    "owner": "Huy Nguyen",
    "period_start": "04-09-2020",
    "period_end": "04-09-2020",
    "time": "04-09-2020",
    "pet": "Dog",
    "status": "Finished",
    "fee": 50,
    "user_id": 1
  },
  {
    "id": 2,
    "owner": "Huy Nguyen",
    "period_start": "03-09-2020",
    "period_end": "03-09-2020",
    "time": "03-09-2020",
    "pet": "Dog",
    "status": "On Going",
    "fee": 50,
    "user_id": 1
  },
  {
    "id": 3,
    "owner": "Hoang Nguyen",
    "period_start": "03-09-2020",
    "period_end": "03-09-2020",
    "time": "03-09-2020",
    "pet": "Dog",
    "status": "On Going",
    "fee": 50,
    "user_id": 2
  }
];

describe("Testing BookingList component existance", () =>{    
    it("The table booking list exists" ,  () => {
        const wrapper= shallow(<BookingList bookings= {bookings} isLoading= {false}/>);
        expect(wrapper.find("div").find(".mt-4").length).toBe(1);
    });
    it("The table has 3 row" ,  () => {
      const wrapper= shallow(<BookingList bookings= {bookings} isLoading= {false}/>);
      expect(wrapper.find("div").find(".mt-4").find("tbody").find("tr").length).toBe(3);
  });
});