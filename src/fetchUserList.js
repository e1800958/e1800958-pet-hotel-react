import React, { Component } from 'react';
import UserList from './UserList';


class fetchUserList extends Component {

  constructor(props) {
    super(props);
    this.state = {users: [], isLoading: true};
    // this.remove = this.remove.bind(this);
  }

  async componentWillMount () {
    this.setState({isLoading: true});

    fetch('https://e1800958-pet-hotel.herokuapp.com/users',{
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: "Basic " + btoa("user:password123"),
      },
    })
        .then(response => response.json())
      .then(data => this.setState({users: data, isLoading: false}));
      
  }

  // async remove(id) {
  //   await fetch('/user/'+id, {
  //     method: 'DELETE',
  //     headers: {
  //       'Accept': 'application/json',
  //       Authorization: "Basic " + btoa("user:password123"),
  //     }
  //   }).then(() => {
  //     let updatedUsers = [...this.state.users].filter(i => i.id !== id);
  //     this.setState({bookings: updatedUsers});
  //   });
  // }

  render() {
    const {users, isLoading} = this.state;
    console.log(this.state.users);

    if (isLoading) {
      return <p>Loading...</p>;
    }
    return (
        <UserList users= {users} isLoading= {false}/>
    );
  }
}

export default fetchUserList;