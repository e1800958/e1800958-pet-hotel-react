import React from 'react';
import ReactDOM from 'react-dom';
import {mount, configure, shallow} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import UserList from './UserList';
import nock from 'nock';
import waitUntil from 'async-wait-until';
import BookingListByUser from './BookingListByUser';

configure({adapter: new Adapter()});

const bookings=[
    {
      "id": 3,
      "owner": "Hoang Nguyen",
      "period_start": "03-09-2020",
      "period_end": "03-09-2020",
      "time": "03-09-2020",
      "pet": "Dog",
      "status": "On Going",
      "fee": 50,
      "user_id": 2
    }
  ];

describe("Testing BookingByUser component existance", () =>{
    it("The table BookingByUser exists" ,  () => {
        const wrapper= shallow(<BookingListByUser bookings={bookings} isLoading={false}/>);
        expect(wrapper.find("div").find(".mt-4").length).toBe(1);
    });

    it("The table BookingByUser has 1 row" ,  () => {
      const wrapper= shallow(<BookingListByUser bookings={bookings} isLoading={false}/>);
      expect(wrapper.find("div").find(".mt-4").find("tbody").find("tr").length).toBe(1);
  });
});
