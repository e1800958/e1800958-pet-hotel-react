import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import fetchUserList from './fetchUserList';
import BookingEdit from './BookingEdit';
import fetchBookingList from './fetchBookingList';
import fetchBookingListByUser from './fetchBookingListByUser';



class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={Home}/>
          {/* <Route path='/bookings' exact={true} component={() => <BookingList bookings={this.state.bookings } isLoading={this.state.isLoading}/>}/> */}
          <Route path='/booking/:id' component={BookingEdit}/>
          <Route path='/bookings' exact={true} component={fetchBookingList}/>
          {/* <Route path='/users' exact={true} component={() => <UserList users={this.state.users}  isLoading={this.state.isLoading}/>  }/>  */}
          <Route path='/users' exact={true} component={fetchUserList}/> 
          <Route path='/booking-user/:id' component={fetchBookingListByUser}/> 
        </Switch>
      </Router>
    )
  }
}


export default App;
